# load library
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  

f = np.array([2, 6, 4])

# decompose A to D and R matrices

D = np.array([[-3, 0, 0],[0, -3, 0],[0, 0, 4]])

R = np.array([[0, 1, 2], [-1, 0, 1], [-1, 3, 0]])


## Test add a line to the file in branch.

# inverse of D matrix
D_inv = np.linalg.inv(D)
    

# initial guess value of x
x = np.array([0, 0, 0])

norm= 0

# walltime before matrix solving
cpu_tm_bg = tm.time()

# iteration
for iter in range(200):
    # use function np.matmul for matrix-vector multiplication
	Rx = np.matmul(R, x)
	b = D_inv
	c = f - Rx
	x_iter = np.matmul(b, c)

    # residual during iterations

	Ax = np.matmul(A, x_iter)
	r = f - Ax

    # use calculate the norm of the residual using numpy function

	for k in range(len(r)):
		norm = norm + r[k]**2



	x = x_iter


norm_final = norm**(1/2)    

# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, norm_final)

